<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
  <head>
    <title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>

</head>

<body<?php print phptemplate_body_class($sidebar_left, $sidebar_right) ?><?php print $onload_attributes ?>>

<div id="wrapper">

	<div id="header"<?php print (count($secondary_links) ? ' class="p2"' : ' class="p1"') ?>>
	<div id="qg"></div>
	<div id="qb"></div>
		<div id="branding">

			  <?php if ($site_name){ ?>
			  	<h1><a href="<?php print url() ?>" title="<?php print t('Home') ?>" rel="home"><?php print $site_name ?></a></h1>
			  	<?php } ?>
			  	<?php if ($site_slogan){ ?>
			  	<div class="slogan"><?php print $site_slogan ?></div>
			  <?php } ?>
		</div><!--branding-->
	<div id="qbl">
			<?php if($search_box) { ?>
			<?php print $search_box ?>
		  <?php } ?>
	</div>
		  <?php if ($header): ?>
		  <div id="header-additional">
		  <?php print $header ?>
		  </div><!--header-additional-->
		  <?php endif; ?>
	</div><!--header-->


		  <div id="menu">
			  <?php if ($primary_links){ ?>
				<?php print theme('links', $primary_links, array('class' => 'links primary_menu')) ?>
			  <?php } ?>
		  </div><!--menu-->
		  <div id="logo1"><?php if ($logo){ ?>
				<img src="<?php print check_url($logo) ?>" alt="<?php print $site_title ?>" id="logo" />
			  <?php } ?>
		  </div>

	<div id="wrapper2">
	<div id="content">
		<!--<?php if ($sidebar_left != ''){ ?>
		  <div id="sidebar-left" class="sidebar">
		  <div class="linea"></div>
		  <?php print $sidebar_left; ?>
		  </div>
		<?php } ?>-->

		<div id="main">
			<div id="c_main">
				<?php if ($breadcrumb){ ?>
				  <div id="breadcrumb"><?php print $breadcrumb ?></div>
				<?php } ?>
				<?php if ($messages != ""){ ?>
				  <div id="message"><?php print $messages ?></div>
				<?php } ?>
				<?php if ($mission != ""){ ?>
				  <div id="mission"><span><?php print $mission ?></span></div>
				<?php } ?>
				<?php if ($title != ""){ ?>

				<?php if (count($secondary_links)){ ?>
					<?php print theme('links', $secondary_links, array('class' => 'links secondary_menu')) ?>
				<?php } ?>
				  <h2 id="title"><?php print $title ?></h2>
				<?php } ?>
				<?php if ($help != ""){ ?>
				  <p id="help"><?php print $help ?></p>
				<?php } ?>
				<?php if ($tabs != ""){ ?>
				  <?php print $tabs ?>
				<?php } ?>
				<div class="content">
				<?php print $before_content ?>
				<?php print $content ?>
				</div><!--content-->
			</div><!--c_main-->
		</div><!--main-->

					<?php if ($sidebar_right != ''){ ?>
					  <div id="sidebar-right" class="sidebar">
					  <?php print $sidebar_right; ?>
					  </div>
					<?php } ?>
	</div><!--content-->

	<div id="footer" class="footer">
			<?php if ($footer_message){ ?>
			  <?php print $footer_message;?>
			<?php } ?>
			  <p class="credits">Powered by <a href="http://www.drupal.org">Drupal</a> and the <a href="http://www.drupal.org/project/barlow">Mondrian</a> theme by <a href="http://egonbianchet.net">Ana...</a></p>
		</div><!--footer-->


	</div><!--wrapper2-->
</div><!--wrapper-->

<?php print $closure;?>
</body>
</html>
