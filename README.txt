
Mondrian Theme for Drupal 5.x designed by Ana Maria Paz Franchini (Ampafra) for the stage "Development and Management of Internet Sites" in Kinetikon Srl (Turin - Italy) on 2007.
This theme validates as XHTML 1.0 Strict and valid CSS 2.1 (tableless)
Supports only the right sidebar
Fixed widthThe Wall template is based on the Barlow template
Godd for blogs
Suport all theme options (slogan, logo, primary/secondary links, mission, etc)
Work fine in IE/Firefox-Mozilla/Opera/Konqueror/Safari.

Live demo: http://www.kinetikon.com/mondrian
Large Screenshot: http://www.kinetikon.com/mondrian/screenshot-drupal.org.jpg

